class HomepagesController < ApplicationController

	def index
		@blogs = BlogIntegration.get_cached_blogs
	end

end
