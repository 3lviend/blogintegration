require 'open-uri'
require 'nokogiri'
require 'net/http'

class BlogIntegration
  attr_accessor :id, :title, :content, :category, :image, :created_at, :slug

  def initialize(options={})
    @id  			  = options[:id]
    @title 			= options[:title]
    @content 		= options[:content]
    @category 	= options[:category]
    @image 			= options[:image]
    @created_at = options[:created_at]
    @slug 		  = options[:slug]
  end

  def self.get_request(url)
    url_parse = URI.parse(url)
    response  = Net::HTTP.get_response(url_parse)
    JSON.parse(response.body)
  end

  def self.blog_request
  	url = "http://blog.zippycrowd.com/wp-json/wp/v2/posts?filter[cat]=1&filter[cat]=138"
    respon = get_request(url)
    results = []
    has_image = nil

    unless respon.blank?
      respon.sort_by{|data| data['date'] }.reverse.first(4).each_with_index do |json, index|
        has_image = json["_links"]["wp:featuredmedia"]

        if has_image
          featuredmedia 		 = json["_links"]["wp:featuredmedia"][0]["href"]
          load_featuredmedia = get_request(featuredmedia)
          image 						 = load_featuredmedia.blank? ? "" : load_featuredmedia["guid"]["rendered"]
        else
          image = ""
        end
        
        open_blog = Nokogiri::HTML(open(json['guid']['rendered']))
        scraping  = open_blog.css('div .et_pb_text')
        blog = BlogIntegration.new(
        	:id 				=> json["id"], 
        	:title 			=> json["title"]["rendered"], 
        	:content 		=> scraping.blank? ? 'No Content' : scraping.first.content, #json["content"]["rendered"],
        	:category 	=> categories(json["categories"]),
          :image 			=> image, 
          :created_at	=> json["date"],
          :slug 			=> json["slug"]
        )

        results << blog
      end
    end

    return results
  end

  def self.get_cached_blogs
    Rails.cache.fetch('zippycrowd_blog', :expires_in => 1.day) do
      blog_request
    end
  end

  def self.categories(cat_id)
  	blog_categories = ""

   	cat_id.each do |catid|
  		loadCat  = get_request('http://blog.zippycrowd.com/wp-json/wp/v2/categories')
  		cat_name = loadCat.find{|cat| cat if cat['id'].eql?(catid)}
  		blog_categories = cat_name.blank? ? "Zippycrowd News" : cat_name['name']
  	end

  	return blog_categories
  end

  def self.clear_cached_blogs
    Rails.cache.clear('zippycrowd_blog')
  end
end
